#language: pt

Funcionalidade: Alterar uma simulação através do CPF

Cenario: Alterar os dados de uma simulação
	Dado que acesso a rota de simulações da API
	Quando altero os campo de uma simulaçao existente
	Então a API retorna status <200>
	E A API retorna a simulação com os dados atualizados
	
Cenario: Alterar os dados de uma simulação com um CPF inexistente 
	Dado que acesso a rota de simulações da API
	Quando altero os campo utilizando um CPF que não possui simulação
	Então a API retorna status <404>
	E a mensagem "CPF não encontrado"
