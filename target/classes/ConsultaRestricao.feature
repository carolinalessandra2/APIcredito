#language: pt

Funcionalidade: Consultar uma restrição pelo CPF

	@Test
	Esquema do Cenario: Consultar um CPF sem restição
		Dado que acesso a rota de restrição da API
		Quando informo o cpf "<CPF>" sem restrição
		Entao a API retorna o status <status>
		Exemplos: 
			| CPF  				| status	|
    	| 66414919004 |    204	|
	
	@Test
	Esquema do Cenario: Consultar um CPF com restrição
	Dado que acesso a rota de restrição da API
	Quando informo um "<CPF>" com restrição
	Entao a API retorna o status <status>
	E retorna a mensagem O CPF "<CPF>" tem problema
	
	Exemplos:
		| CPF  				| status	|
    | 97093236014 |    200	|
    | 60094146012 |    200	|
    | 84809766080 |    200	|
    | 62648716050 |    200	|
    | 26276298085 |    200	|
	