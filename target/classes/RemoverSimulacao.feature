#language: pt

Funcionalidade: Remover uma simulação cadastrada pelo seu ID

#teste altera os dados de simulação do ID 38
Cenario: Remover uma simulação cadastrada
	Dado que acesso a rota de simulações da API
	Quando removo uma simulação cadastrada
	Então a API retorna status <200>
	
#A API esta retornando 200	
Cenario: Remover uma simulação que não esta cadastrada
	Dado que acesso a rota de simulações da API
	Quando removo uma simulação que não esta cadastrada 
	Então a API retorna status <404>