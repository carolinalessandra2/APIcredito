#language: pt

Funcionalidade: Criar uma simulação 

#Na validação de cenários os campos a serem enviados são: CPF, NOME, EMAIL, VALOR,
#PARCELA e SEGURO

Cenario: Inserir uma nova simulação
	Dado que acesso a rota de simulações da API
	Quando envio os campos obrigatórios corretamente 
	Então uma nova simulação é criada
	
Cenario: Inserir uma simulação com um CPF já existente 
	Dado que acesso a rota de simulações da API
	Quando envio os campos obrigatórios com um CPF já cadastrado 
	Então a API retorna status <409>
	E a mensagem de "CPF duplicado"

#Na ausência do campo SEGURO a API esta retornando erro 500
Cenario: Validar obrigatoriedade dos campos
	Dado que acesso a rota de simulações da API
	Quando envio apenas o campo SEGURO
	Então a API retorna status <400>
	E uma lista de erros

#A API esta permitindo o envio no formato 999.999.999-99
Cenario: Validar formato de envio do campo CPF
	Dado que acesso a rota de simulações da API
	Quando envio uma simulação com o CPF no formato 999.999.999-99
	Então a API retorna status <400>
		