#language: pt

Funcionalidade: Consultar simulação

Cenario: Consultar todas as simulações
	Dado que acesso a rota de simulações da API
	Quando busco por todas as simulações
	Então a API retorna status <200>
	
Cenario: Consultar uma simuação pelo CPF
	Dado que acesso a rota de simulações da API
	Quando busco por uma simulação 
	Então a API retorna status <200>
	
#busca feita pelo CPF 66414919004
Cenario: Validar status quando não houver simulações 
	Dado que acesso a rota de simulações da API
	Quando busco por todas as simulações 
	Então a API retorna status <204>
	
	Cenario: Validar status quando o CPF não possuir simulação
	Dado que acesso a rota de simulações da API
	Quando busco por uma simulação 
	Então a API retorna status <404>
	