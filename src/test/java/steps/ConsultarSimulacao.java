package steps;

import io.cucumber.java.pt.Quando;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ConsultarSimulacao {

	RequestSpecification request;
	Response response;

	@Quando("busco por todas as simulações")
	public void busco_por_todas_as_simulações() {
		RestAssured.given()
		.when()
		.get("/simulacoes");
	}

	@Quando("busco por uma simulação")
	public void busco_por_uma_simulação() {
		RestAssured.given()
		.when()
		.get("/simulacoes/66414919004");
	}

}
