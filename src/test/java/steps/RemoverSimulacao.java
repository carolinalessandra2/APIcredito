package steps;

import io.cucumber.java.BeforeAll;
import io.cucumber.java.pt.Quando;
import io.restassured.RestAssured;
import com.github.javafaker.Faker;

public class RemoverSimulacao {

	private String requestBody;

	static Faker faker;
	String cpf = faker.numerify("###########");
	String nome = faker.name().firstName();
	String email = faker.internet().emailAddress();
	int valor = faker.number().numberBetween(1000, 4000);
	int parcelas = faker.number().numberBetween(2, 48);
	boolean seguro = faker.bool().bool();
	   
	
	@BeforeAll
	    public static void setUp() {
	        faker = new Faker();
	    }

	@Quando("removo uma simulação cadastrada")
	public void removo_uma_simulação_cadastrada() {
		//Cria a publicação 
		requestBody = "{ \"cpf\": \"" + cpf + "\", \"nome\": \"" + nome + "\", \"email\": \"" + email + "\", \"valor\": " + valor + ", \"parcelas\": " + parcelas + ", \"seguro\": " + seguro + " }";
		int id = RestAssured.given()
				.contentType("application/json")
				.body(requestBody)
				.post("/simulacoes")
				.then()
				.extract()
				.path("id");

		RestAssured.given()
		.when()
		.delete("/simulacoes/" + id)
		.then();
	}

	@Quando("removo uma simulação que não esta cadastrada")
	public void removo_uma_simulação_que_não_esta_cadastrada() {
		RestAssured.given()
        .when()
        .delete("/simulacoes/000000")
        .then()
        .statusCode(404);
	}

}
