package steps;

import static org.hamcrest.Matchers.*;

import io.cucumber.java.BeforeAll;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import com.github.javafaker.Faker;


public class AlterarSimulacao {

	Response response;
	RequestSpecification request;
	private String requestBody;
	static Faker faker;

	@BeforeAll
	public static void setUp() {
		faker = new Faker();
	}
	String cpf = faker.numerify("###########");
	String nome = faker.name().firstName();
	String email = faker.internet().emailAddress();
	int valor = faker.number().numberBetween(1000, 4000);
	int parcelas = faker.number().numberBetween(2, 48);
	boolean seguro = faker.bool().bool();


	//teste altera os dados de ID 38
	@Quando("altero os campo de uma simulaçao existente")
	public void altero_todos_os_campo_da_simulaçao() {
		//Cria a publicação 
		requestBody = "{ \"cpf\": \"" + cpf + "\", \"nome\": \"" + nome + "\", \"email\": \"" + email + "\", \"valor\": " + valor + ", \"parcelas\": " + parcelas + ", \"seguro\": " + seguro + " }";
		String cpf = RestAssured.given()
				.contentType("application/json")
				.body(requestBody)
				.post("/simulacoes")
				.then()
				.extract()
				.path("cpf");
		
		RestAssured.given()
		.contentType("application/json")
		
		.body("{ \"cpf\": \"" + cpf + "\", \"nome\": \"" + "nomeedit" + "\", \"email\": \"" + email + "\", \"valor\": " + valor + ", \"parcelas\": " + parcelas + ", \"seguro\": " + seguro + " }").log().all()
		.when()
		.put("/simulacoes/" + cpf);
	}

	@Então("A API retorna a simulação com os dados atualizados")
	public void a_api_retorna_a_simulação_com_os_dados_atualizados() {		
		response.then()
		.assertThat()
		.body("cpf", equalTo(cpf))
		.body("nome", not(equalTo(nome)))
		.body("email", equalTo(email))
		.body("valor", equalTo(valor))
		.body("parcelas", equalTo(parcelas))
		.body("seguro", equalTo(seguro));
	}

}
