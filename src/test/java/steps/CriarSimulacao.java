package steps;

import io.cucumber.java.BeforeAll;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import static org.hamcrest.Matchers.*;
import com.github.javafaker.Faker;

public class CriarSimulacao {
	
	Response response;
	RequestSpecification request;
	private String requestBody;
	
	static Faker faker;

    @BeforeAll
    public static void setUp() {
        faker = new Faker();
    }
    
	String cpf = faker.numerify("###########");
	String nome = faker.name().firstName();
	String email = faker.internet().emailAddress();
	int valor = faker.number().numberBetween(1000, 4000);
	int parcelas = faker.number().numberBetween(2, 48);
	boolean seguro = faker.bool().bool();

	@Dado("que acesso a rota de simulações da API")
	public void que_acesso_a_rota_de_simulações_da_api() {
		request = RestAssured.given()
				.contentType(ContentType.JSON);
	}

	@Quando("envio os campos obrigatórios corretamente")
	public void envio_os_campos_obrigatórios_corretamente() {
		requestBody = "{ \"cpf\": \"" + cpf + "\", \"nome\": \"" + nome + "\", \"email\": \"" + email + "\", \"valor\": " + valor + ", \"parcelas\": " + parcelas + ", \"seguro\": " + seguro + " }";
		response = request
				.body(requestBody)
				.when()
				.post("/simulacoes");
	}

	@Entao("uma nova simulação é criada")
	public void uma_nova_simulação_é_criada() {
		response.then()
		.statusCode(201)
		.assertThat()
		.body("cpf", equalTo(cpf))
		.body("nome", equalTo(nome))
		.body("email", equalTo(email))
		.body("valor", equalTo(valor))
		.body("parcelas", equalTo(parcelas))
		.body("seguro", equalTo(seguro));
	}

	@Quando("envio os campos obrigatórios com um CPF já cadastrado")
	public void envio_os_campos_obrigatórios_com_um_cpf_já_cadastrado() {
		response = request
				.body("{ \"cpf\": \"66414919004\", \"nome\": \"" + nome + "\", \"email\": \"" + email + "\", \"valor\": " + valor + ", \"parcelas\": " + parcelas + ", \"seguro\": " + seguro + " }")
				.when()
				.post("/simulacoes");
	}

	@Entao("a API retorna status <{int}>")
	public void a_api_retorna_status(int status) {
		request.then().statusCode(status);
	}

	@Entao("a mensagem de {string}")
	public void a_mensagem_de(String msg) {
		response.then().body("mensagem", equalTo(msg));
	}

	@Quando("envio apenas o campo SEGURO")
	public void envio_apenas_o_campo_seguro() {
		response = request
				.body("{ \"seguro\": " + seguro + " }")
				.when()
				.post("/simulacoes");
	}

	@Entao("uma lista de erros")
	public void uma_lista_de_erros() {
		response.then()
		.assertThat()
		.body("erros.parcelas", equalTo("Parcelas não pode ser vazio"))
		.body("erros.cpf", equalTo("CPF não pode ser vazio"))
		.body("erros.valor", equalTo("Valor não pode ser vazio"))
		.body("erros.nome", equalTo("Nome não pode ser vazio"))
		.body("erros.email", equalTo("E-mail não deve ser vazio"));
	}

	@Quando("envio uma simulação com o CPF no formato {double}-{int}")
	public void envio_uma_simulação_com_o_cpf_no_formato(Double double1, Integer int1) {
	}


}
