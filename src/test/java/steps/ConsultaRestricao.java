package steps;

import io.cucumber.java.Before;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static org.hamcrest.Matchers.*;

import java.net.URISyntaxException;

public class ConsultaRestricao {

	Response response;
	RequestSpecification request;

	@Before
	public static void setup() {
		RestAssured.baseURI = "http://localhost:8080/";
		RestAssured.basePath = "/api/v1";
	}

	@Dado("que acesso a rota de restrição da API")
	public void que_acesso_a_rota_de_restrição_da_api() {
		request = RestAssured.given();

	}

	@Quando("informo o cpf {string} sem restrição")
	public void informo_o_cpf_sem_restrição(String cpf) {
		response = request.when()
				.get("/restricoes/" + cpf);
	}

	@Entao("a API retorna o status {int}")
	public void a_api_retorna_o_status(int status) {
		response.then()
		.statusCode(status);
	}

	@Quando("informo um {string} com restrição")
	public void informo_um_com_restrição(String cpf) {
		response = request.when()
				.get("/restricoes/" + cpf);
	}

	@Entao("retorna a mensagem O CPF {string} tem problema")
	public void retorna_a_mensagem_o_cpf_tem_problema(String cpf) {
		response.then()
		.assertThat()
		.body("mensagem", is("O CPF " + cpf + " tem problema"));
	}

}
